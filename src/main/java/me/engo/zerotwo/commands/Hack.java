package me.engo.zerotwo.commands;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Random;

import com.google.gson.JsonParser;
import me.engo.zerotwo.Config;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

public class Hack extends ListenerAdapter {
	
	public static String alias = "virus";
	
	@Override
	public void onGuildMessageReceived(GuildMessageReceivedEvent Context) {
		Config c = new Config();
		String[] messageSent = Context.getMessage().getContentRaw().split(" ");
		if (Context.getAuthor().isBot()) return;
		
		if (messageSent[0].equalsIgnoreCase(c.prefix + "hack") || messageSent[0].equalsIgnoreCase(c.prefix + alias)) {

			try {
				String language;
				File languages = new File("Database/Language/" + Context.getAuthor().getId());
				if (languages.exists()) {
					File[] languages_ = languages.listFiles();
					assert languages_ != null;
					language = languages_[0].getName();
				} else {
					language = "english_en";
				}

				if (messageSent.length < 2) {
					String text = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("basic_warnings").getAsJsonObject().get("low_parameters").getAsString();
					Context.getChannel().sendMessage(text).queue();
				} else if (messageSent.length > 2) {
					String text = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("basic_warnings").getAsJsonObject().get("high_parameters").getAsString();
					Context.getChannel().sendMessage(text).queue();
				} else {
					try {
						String text1 = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("logs").getAsJsonObject().get("hack_IP_1").getAsString();
						String text2 = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("logs").getAsJsonObject().get("hack_IP_2").getAsString();
						String text3 = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("logs").getAsJsonObject().get("hack_nudes_1").getAsString();
						String text4 = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("logs").getAsJsonObject().get("hack_nudes_2").getAsString();
						String text5 = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("logs").getAsJsonObject().get("hack_money_1").getAsString();
						String text6 = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("logs").getAsJsonObject().get("hack_money_2").getAsString();
						String text7 = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("logs").getAsJsonObject().get("hack_complete").getAsString();

						Random rnd = new Random();
						User u = Context.getMessage().getMentionedUsers().get(0);

						Context.getChannel().sendMessage(text1 + u.getName() + text2).queue();
						Thread.sleep((rnd.nextInt(8)) * 1000);

						Context.getChannel().sendMessage(text3 + u.getName() + text4).queue();
						Thread.sleep((rnd.nextInt(8)) * 1000);

						Context.getChannel().sendMessage(text5 + u.getName() + text6).queue();
						Thread.sleep((rnd.nextInt(8)) * 1000);

						Context.getChannel().sendMessage(text7).queue();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			} catch (FileNotFoundException e){
				e.printStackTrace();
			}
		}
	}

}

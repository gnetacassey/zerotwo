package me.engo.zerotwo.commands;

import com.google.gson.JsonParser;
import me.engo.zerotwo.Config;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Objects;

public class Profile extends ListenerAdapter {

    public static String alias = "me";

    @Override
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        Config c = new Config();
        String[] messageSent = event.getMessage().getContentRaw().split(" ");
        if (event.getAuthor().isBot()) return;

        if (messageSent[0].equalsIgnoreCase(c.prefix + "profile") || messageSent[0].equalsIgnoreCase(c.prefix + alias)) {

            try {
                String language;
                File languages = new File("Database/Language/" + event.getAuthor().getId());
                if (languages.exists()) {
                    File[] languages_ = languages.listFiles();
                    assert languages_ != null;
                    language = languages_[0].getName();
                } else {
                    language = "english_en";
                }

                EmbedBuilder em = new EmbedBuilder();
                if (messageSent.length < 2) {
                    File marry = new File("Database/Marry/" + event.getAuthor().getId());

                    em.setTitle("Profile");
                    em.addField("Name", event.getAuthor().getAsMention(), false);
                    em.setThumbnail(event.getAuthor().getAvatarUrl());
                    em.setFooter(c.footer1, c.footer2);
                    em.setColor(new Color(c.Color));

                    if (marry.exists()) {
                        File[] a = marry.listFiles();
                        assert a != null;
                        String names = a[0].getName();
                        String name = Objects.requireNonNull(event.getJDA().getUserById(names)).getAsTag();
                        em.addField("Married", name, false);
                    } else {
                        em.addField("Married", "No", false);
                    }

                    if (event.getAuthor().getId().equals("574992310048260097")) {
                        em.addField("Badges", "Bot Owner, Developer", false);
                        em.setImage("https://api.alexflipnote.dev/supreme?text=" + event.getAuthor().getName());
                    } else {
                        em.addField("Badges", "None", false);
                    }
                    event.getChannel().sendMessage(em.build()).queue();
                } else if (messageSent.length == 2) {

                    User u = event.getMessage().getMentionedUsers().get(0);

                    File marry = new File("Database/Marry/" + u.getId());

                    em.setTitle("Profile");
                    em.addField("Name", u.getAsMention(), false);
                    em.setThumbnail(u.getAvatarUrl());
                    em.setFooter(c.footer1, c.footer2);
                    em.setColor(new Color(c.Color));

                    if (marry.exists()) {
                        File[] a = marry.listFiles();
                        assert a != null;
                        String names = a[0].getName();
                        String name = Objects.requireNonNull(event.getJDA().getUserById(names)).getAsTag();
                        em.addField("Married", name, false);
                    } else {
                        em.addField("Married", "No", false);
                    }

                    if (u.getId().equals("574992310048260097")) {
                        em.addField("Badges", "Bot Owner, Developer", false);
                        em.setImage("https://api.alexflipnote.dev/supreme?text=" + u.getName());
                    } else {
                        em.addField("Badges", "None", false);
                    }
                    event.getChannel().sendMessage(em.build()).queue();
                } else {
                    String text = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("basic_warnings").getAsJsonObject().get("high_parameters").getAsString();
                    event.getChannel().sendMessage(text).queue();
                }
            } catch (FileNotFoundException e){
                e.printStackTrace();
            }
        }
    }
}

package me.engo.zerotwo.commands;

import com.google.gson.JsonParser;
import me.engo.zerotwo.Config;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class Count extends ListenerAdapter {

	public static String alias = "write";
	
	@Override
	public void onGuildMessageReceived(GuildMessageReceivedEvent Context) {
		Config c = new Config();
		String[] messageSent = Context.getMessage().getContentRaw().split(" ");
		if (Context.getAuthor().isBot()) return;
		
		if (messageSent[0].equalsIgnoreCase(c.prefix + "count") || messageSent[0].equalsIgnoreCase(c.prefix + "write")) {

			try {
				String language;
				File languages = new File("Database/Language/" + Context.getAuthor().getId());
				if (languages.exists()) {
					File[] languages_ = languages.listFiles();
					assert languages_ != null;
					language = languages_[0].getName();
				} else {
					language = "english_en";
				}

				if (messageSent.length <= 2) {
					String text = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("basic_warnings").getAsJsonObject().get("low_parameters").getAsString();
					Context.getChannel().sendMessage(text).queue();
				} else  if (messageSent.length > 3) {
					String text = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("basic_warnings").getAsJsonObject().get("high_parameters").getAsString();
					Context.getChannel().sendMessage(text).queue();
				} else {

					if (Context.getChannel().getName().contains("counting") || Context.getChannel().getName().contains("spam")) {
						int a = Integer.parseInt(messageSent[2]);
						int b = Integer.parseInt(messageSent[1]);

						if (Context.getMember() == Context.getGuild().getOwner()) {

							if (a - b <= 20) {
								for (int i = b; i <= a; i = i + 1) {

									Context.getChannel().sendMessage(i + ".").queue();

								}
							} else {

								String text = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("advanced_warnings").getAsJsonObject().get("count_owner_high").getAsString();

								Context.getChannel().sendMessage(text).queue();

							}

						} else {

							if (a - b <= 10) {
								for (int i = b; i <= a; i = i + 1) {

									Context.getChannel().sendMessage(i + ".").queue();

								}
							} else {

								String text = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("advanced_warnings").getAsJsonObject().get("count_member_high").getAsString();

								Context.getChannel().sendMessage(text).queue();

							}

						}
					} else {
						String text = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("advanced_warnings").getAsJsonObject().get("count_invalid_channel").getAsString();
						Context.getChannel().sendMessage(text).queue();
					}
				}
			} catch (FileNotFoundException e){
				e.printStackTrace();
			}
		}
	}
}

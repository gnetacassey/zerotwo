package me.engo.zerotwo.commands;

import com.google.gson.JsonParser;
import me.engo.zerotwo.Config;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Event extends ListenerAdapter {
	
	public static String alias = "There's no aliases.";

	public File f = new File("Database/Event/true");
	public File fi = new File("Database/Event/false");
	
	@Override
	public void onGuildMessageReceived(GuildMessageReceivedEvent Context) {
		Config c = new Config();
		String[] messageSent = Context.getMessage().getContentRaw().split(" ");
		if (Context.getAuthor().isBot()) return;
		
		if (messageSent[0].equalsIgnoreCase(c.prefix + "event")) {

			try {
				String language;
				File languages = new File("Database/Language/" + Context.getAuthor().getId());
				if (languages.exists()) {
					File[] languages_ = languages.listFiles();
					assert languages_ != null;
					language = languages_[0].getName();
				} else {
					language = "english_en";
				}

				if (messageSent.length < 2) {

					boolean active;
					active = f.exists();
					if (active) {
						String text = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("logs").getAsJsonObject().get("event_on").getAsString();
						Context.getChannel().sendMessage(text).queue();
					} else {
						String text = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("logs").getAsJsonObject().get("event_off").getAsString();
						Context.getChannel().sendMessage(text).queue();
					}
				} else if (messageSent.length == 2) {
					if (Context.getAuthor().getId().equals("574992310048260097")) {
						switch (messageSent[1].toLowerCase()) {
							case "set":
								try {
									fi.delete();
									f.createNewFile();
									Context.getChannel().sendMessage("Event was started.").queue();
								} catch (IOException e) {
									e.printStackTrace();
								}
								break;

							case "del":
								try {
									f.delete();
									fi.createNewFile();
									Context.getChannel().sendMessage("Event was deleted.").queue();
								} catch (IOException e) {
									e.printStackTrace();
								}
								break;

							default:
								Context.getChannel().sendMessage("Invalid args[1]. '" + messageSent[1] + "'").queue();
								break;
						}
					} else {
						String text = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("basic_warnings").getAsJsonObject().get("developer_false").getAsString();
						Context.getChannel().sendMessage(text).queue();
					}
				} else {
					String text = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("basic_warnings").getAsJsonObject().get("high_parameters").getAsString();
					Context.getChannel().sendMessage(text).queue();
				}
			} catch (FileNotFoundException e){
				e.printStackTrace();
			}
		}
	}
}

package me.engo.zerotwo.commands;

import com.google.gson.JsonParser;
import com.sedmelluq.discord.lavaplayer.player.AudioLoadResultHandler;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.player.DefaultAudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.source.AudioSourceManagers;
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException;
import com.sedmelluq.discord.lavaplayer.track.AudioPlaylist;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import me.engo.zerotwo.Bot;
import me.engo.zerotwo.Config;
import me.engo.zerotwo.GuildMusicManager;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.managers.AudioManager;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class MUSIC extends ListenerAdapter {

    public static String alias = "This isn't command.";

    private final AudioPlayerManager playerManager;
    private final Map<Long, GuildMusicManager> musicManagers;

    public static String language;

    public static Member member;
    public static String id;

    public String music1;
    public String music2;
    public String music3;
    public String music4;
    public String music5;

    public MUSIC() {
        this.musicManagers = new HashMap<>();

        this.playerManager = new DefaultAudioPlayerManager();
        AudioSourceManagers.registerRemoteSources(playerManager);
        AudioSourceManagers.registerLocalSource(playerManager);
    }

    private synchronized GuildMusicManager getGuildAudioPlayer(Guild guild) {
        long guildId = Long.parseLong(guild.getId());
        GuildMusicManager musicManager = musicManagers.get(guildId);

        if (musicManager == null) {
            musicManager = new GuildMusicManager(playerManager);
            musicManagers.put(guildId, musicManager);
        }

        guild.getAudioManager().setSendingHandler(musicManager.getSendHandler());

        return musicManager;
    }

    @Override
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        Config c = new Config();
        String[] args = event.getMessage().getContentRaw().split(" ");
        if (event.getAuthor().isBot()) return;

        try {
            File languages = new File("Database/Language/" + event.getAuthor().getId());
            if (languages.exists()) {
                File[] languages_ = languages.listFiles();
                assert languages_ != null;
                language = languages_[0].getName();
            } else {
                language = "english_en";
            }

            member = event.getMember();
            id = event.getChannel().getId();

            music1 = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("logs").getAsJsonObject().get("music_adding").getAsString();
            music2 = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("logs").getAsJsonObject().get("music_first").getAsString();
            music3 = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("logs").getAsJsonObject().get("music_nothing").getAsString();
            music4 = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("logs").getAsJsonObject().get("music_failed").getAsString();
            music5 = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("logs").getAsJsonObject().get("music_skipped").getAsString();

            if (args[0].equalsIgnoreCase(c.prefix + "play") || args[0].equalsIgnoreCase(c.prefix + alias)) {

                if (args.length < 2) {
                    String text = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("basic_warnings").getAsJsonObject().get("low_parameters").getAsString();
                    event.getChannel().sendMessage(text).queue();
                } else {

                    int a = args.length;

                    String[] slova = new String[a];

                    System.arraycopy(args, 1, slova, 0, a - 1);

                    StringBuilder done = new StringBuilder();

                    for (int i = 0; i < slova.length - 1; i++) {
                        done.append(" ").append(slova[i]);
                    }

                    done = new StringBuilder(done.substring(1));

                    if (done.toString().startsWith(" ")){
                        String done1 = done.toString().substring(1);
                        loadAndPlay(event.getChannel(), done1);
                    } else {
                        loadAndPlay(event.getChannel(), done.toString());
                    }
                }

            } else if (args[0].equalsIgnoreCase(c.prefix + "skip")) {

                if (args.length < 2) {
                    if (Objects.requireNonNull(Objects.requireNonNull(event.getMember()).getVoiceState()).inVoiceChannel()) {
                        if (Objects.requireNonNull(event.getMember()).hasPermission(Permission.MANAGE_CHANNEL) || event.getMember().getPermissions(Objects.requireNonNull(Objects.requireNonNull(event.getMember().getVoiceState()).getChannel())).contains(Permission.MANAGE_CHANNEL)) {
                            skipTrack(event.getChannel());
                        } else {
                            String text = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("basic_warnings").getAsJsonObject().get("permissions_false").getAsString();
                            event.getChannel().sendMessage(text).queue();
                        }
                    } else {
                        String text = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("logs").getAsJsonObject().get("music_false").getAsString();
                        Objects.requireNonNull(Bot.jda.getTextChannelById(id)).sendMessage(text).queue();
                    }
                } else {
                    String text = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("basic_warnings").getAsJsonObject().get("high_parameters").getAsString();
                    event.getChannel().sendMessage(text).queue();
                }

            } else if (args[0].equalsIgnoreCase(c.prefix + "stop")) {
                if (args.length < 2) {
                    if (Objects.requireNonNull(Objects.requireNonNull(event.getMember()).getVoiceState()).inVoiceChannel()) {
                        if (Objects.requireNonNull(event.getMember()).hasPermission(Permission.MANAGE_CHANNEL) || event.getMember().getPermissions(Objects.requireNonNull(Objects.requireNonNull(event.getMember().getVoiceState()).getChannel())).contains(Permission.MANAGE_CHANNEL)) {
                            Guild guild = event.getGuild();
                            AudioManager audioManager = guild.getAudioManager();
                            if (audioManager.isConnected()) {
                                GuildMusicManager musicManager = getGuildAudioPlayer(event.getGuild());
                                musicManager.player.stopTrack();
                                audioManager.closeAudioConnection();

                                String text = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("logs").getAsJsonObject().get("done").getAsString();
                                event.getChannel().sendMessage(text).queue();
                            } else {
                                String text = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("logs").getAsJsonObject().get("music_false_2").getAsString();
                                event.getChannel().sendMessage(text).queue();
                            }
                        } else {
                            String text = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("basic_warnings").getAsJsonObject().get("permissions_false").getAsString();
                            event.getChannel().sendMessage(text).queue();
                        }
                    } else {
                        String text = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("logs").getAsJsonObject().get("music_false").getAsString();
                        Objects.requireNonNull(Bot.jda.getTextChannelById(id)).sendMessage(text).queue();
                    }
                } else {
                    String text = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("basic_warnings").getAsJsonObject().get("high_parameters").getAsString();
                    event.getChannel().sendMessage(text).queue();
                }
            }
        } catch (FileNotFoundException e){
            e.printStackTrace();
        }
    }

    private void loadAndPlay(final TextChannel channel, final String trackUrl) throws FileNotFoundException {

        if (!Objects.requireNonNull(member.getVoiceState()).inVoiceChannel()){
            String text = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("logs").getAsJsonObject().get("music_false").getAsString();
            Objects.requireNonNull(Bot.jda.getTextChannelById(id)).sendMessage(text).queue();
            return;
        }

        GuildMusicManager musicManager = getGuildAudioPlayer(channel.getGuild());

        playerManager.loadItemOrdered(musicManager, trackUrl, new AudioLoadResultHandler() {
            @Override
            public void trackLoaded(AudioTrack track) {
                channel.sendMessage(music1 + track.getInfo().title).queue();

                try {
                    play(channel.getGuild(), musicManager, track);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void playlistLoaded(AudioPlaylist playlist) {
                AudioTrack firstTrack = playlist.getSelectedTrack();

                if (firstTrack == null) {
                    firstTrack = playlist.getTracks().get(0);
                }

                channel.sendMessage(music1 + firstTrack.getInfo().title +  music2 + playlist.getName() + ")").queue();

                try {
                    play(channel.getGuild(), musicManager, firstTrack);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void noMatches() {
                channel.sendMessage(music3 + trackUrl).queue();
            }

            @Override
            public void loadFailed(FriendlyException exception) {
                channel.sendMessage(music4 + exception.getMessage()).queue();
            }
        });
    }

    private void play(Guild guild, GuildMusicManager musicManager, AudioTrack track) throws FileNotFoundException {
        connectToFirstVoiceChannel(guild.getAudioManager());

        musicManager.scheduler.queue(track);
    }

    private void skipTrack(TextChannel channel) {
        GuildMusicManager musicManager = getGuildAudioPlayer(channel.getGuild());
        musicManager.scheduler.nextTrack();

        channel.sendMessage(music5).queue();
    }

    private static void connectToFirstVoiceChannel(AudioManager audioManager) throws FileNotFoundException {
        if (!audioManager.isConnected() && !audioManager.isAttemptingToConnect()) {

            if (Objects.requireNonNull(member.getVoiceState()).inVoiceChannel()){
                audioManager.openAudioConnection(member.getVoiceState().getChannel());
            } else {

                String text = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("logs").getAsJsonObject().get("music_false").getAsString();
                Objects.requireNonNull(Bot.jda.getTextChannelById(id)).sendMessage(text).queue();

            }
        }
    }
}

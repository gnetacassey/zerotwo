package me.engo.zerotwo.commands;

import java.awt.Color;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Arrays;

import com.google.gson.JsonParser;
import me.engo.zerotwo.Config;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

public class Didyoumean extends ListenerAdapter {
	
	public static String alias = "dym";

	@Override
	public void onGuildMessageReceived(GuildMessageReceivedEvent Context) {
		Config c = new Config();
		String[] messageSent = Context.getMessage().getContentRaw().split(" ");
		if (Context.getAuthor().isBot()) return;
		
		if (messageSent[0].equalsIgnoreCase(c.prefix + "didyoumean") || messageSent[0].equalsIgnoreCase(c.prefix + alias)) {

			try {
				String language;
				File languages = new File("Database/Language/" + Context.getAuthor().getId());
				if (languages.exists()) {
					File[] languages_ = languages.listFiles();
					assert languages_ != null;
					language = languages_[0].getName();
				} else {
					language = "english_en";
				}

				if (messageSent.length < 2 || !Arrays.toString(messageSent).contains(";") || messageSent[1].equalsIgnoreCase(";")) {
					String text = JsonParser.parseReader(new FileReader("languages/" + language + ".json")).getAsJsonObject().get("basic_warnings").getAsJsonObject().get("low_parameters").getAsString();
					Context.getChannel().sendMessage(text).queue();
				} else {
					StringBuilder word = new StringBuilder();

					for (int i = 1; i < messageSent.length; i++) {
						word.append("+").append(messageSent[i]);
					}

					word = new StringBuilder(word.toString().replace(";", "�"));

					String firstWord;
					String secondWord;

					firstWord = word.substring(0, word.indexOf("�"));
					secondWord = word.substring(word.indexOf("�") + 1);

					firstWord = firstWord.substring(1);

					char a = secondWord.charAt(0);

					boolean b = String.valueOf(a).equalsIgnoreCase("+");

					if (b) {secondWord = secondWord.substring(1);}

					EmbedBuilder em = new EmbedBuilder();
					em.setImage("https://api.alexflipnote.dev/didyoumean?top=" + firstWord + "&bottom=" + secondWord);
					em.setColor(new Color(c.Color));
					em.setFooter(c.footer1, c.footer2);

					Context.getChannel().sendMessage(em.build()).queue();
				}
			} catch (FileNotFoundException e){
				e.printStackTrace();
			}
		}
	}
	
}

package me.engo.zerotwo;

public class Config {

    public String prefix = ".zt";

    public int Color = 0xff00ff;
    public String footer1 = "Bot developed by ENGO_150#4264";
    public String footer2 = "https://cdn.discordapp.com/avatars/574992310048260097/75cc8d8c5a637c05450092fda25e8576.png";

    public boolean nsfw = true;
    public boolean roleplay = true;
    public boolean moderation = true;

}
